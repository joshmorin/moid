/*
    Moid: Cancelable, Detached Loops and Scheduling
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::thread;
use std::sync::mpsc;

/// ‎‫قبضة خيط التكرار
pub struct MoidHandle {
    join_handle: thread::JoinHandle<()>,
    signal_tx: mpsc::Sender<()>
}
impl MoidHandle {
    /// ‎‫يستوقف الخيط في أقرب فرصة. إن كان الخيط في وسط تنفيذ الدالة المناطة
    /// ‎‫إليه فإنه يستمر إلى نهاية الدالة ثم يقف.
    pub fn cancel(self) {
        let _ = self.signal_tx.send(());
        self.join_handle.thread().unpark();
    }
    /// ‎‫تصد المنفذ إلى أن ينتهي الخيط من عمله.
    pub fn join(self) {
        let _ = self.join_handle.join();
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Time {
    Duration(i64),
    Stamp(i64)
}

/// ‎وصف لخيط التكرار
///
/// ‎‫هذا النوع يصف الخيط فقط ولا يقوم بإنشائه إلى إذا استخدمت إحدى دواله 
/// ‎‫الإنشائية.
#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct MoidSpec {
    from    : Option<Time>,
    to      : Option<Time>,
    count   : Option<usize>, 
    sleep   : f64, 
    inc     : bool
}
impl MoidSpec {
    /// ‎وصف جديد
    pub fn new() -> MoidSpec {
        use std::default::Default;
        Default::default()
    }
    /// ‎‫الوقت الذي يبدأ معه الخيط في تكرار التنفيذ. الوقت يكون عدد الثواني
    /// ‎‫منذ الـepoch بحسب التوقيت المحلي للنظام.
    /// ‎‫
    /// ‎‫تتعارض هذه الدالة مع دالة `delay`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn from(&mut self, timestamp: i64) -> &mut MoidSpec {
        self.from = Some(Time::Stamp(timestamp));
        self
    }
    /// ‎‫عدد الثواني التي ينتظرها الخيط قبل المباشرة في التكرار. يبدأ العد مع
    /// ‎‫إنشاء الخيط (لا تنفيذ هذه الدالة).
    /// ‎‫
    /// ‎‫تتعارض هذه الدالة مع دالة `from`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn delay(&mut self, delay: i64) -> &mut MoidSpec {
        self.from = Some(Time::Duration(delay));
        self
    }
    /// ‎‫الوقت الذي يقف معه الخيط عن تكرار التنفيذ. الوقت يكون عدد الثواني
    /// ‎‫منذ الـepoch بحسب التوقيت المحلي للنظام.
    /// ‎‫
    /// ‎‫تتعارض هذه الدالة مع دالة `duration`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn to(&mut self, timestamp: i64) -> &mut MoidSpec {
        self.to = Some(Time::Stamp(timestamp));
        self
    }
    /// ‎‫عدد الثواني التي يستمر خلالها الخيط في التكرار. يبدأ العد مع
    /// ‎‫أول تكرار.
    /// ‎‫
    /// ‎‫تتعارض هذه الدالة مع دالة `to`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn duration(&mut self, duration: i64) -> &mut MoidSpec {
        self.to = Some(Time::Duration(duration));
        self
    }
    /// ‎‫أقصى عدد المرات التي يكرر فيها الخيط تنفيذ دالته
    pub fn count(&mut self, count: usize) -> &mut MoidSpec {
        self.count = Some(count);
        self
    }
    /// ‎‫الزمن الذي يفصل بين التكرار.
    pub fn sleep(&mut self, sec: f64) -> &mut MoidSpec {
        self.sleep = sec;
        self
    }
    /// ‎‫تُحسَب المدة التي يستغرقها الخيط في تنفيذ دالته ضمن زمن الفاصل بين التكرار.
    /// ‎‫إذا كانت مدة تنفيذ الدالة أكبر من الزمن الفاصل، فإن الخيط سيكرر التنفيذ مباشرة 
    /// ‎‫دون فاصل زمني.
    ///
    ///
    /// ```no_test
    ///     sssssssssssssss|ssssssssssssssss|...
    ///     wwwwww         |wwwwww          |...
    /// ```
    ///
    ///
    /// ‎‫تتعارض هذه الدالة مع دالة `exclusive`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn inclusive(&mut self) -> &mut MoidSpec {
        self.inc = true;
        self
    }
    /// ‎‫الزمن الفاصل بين التكرار ثابت لا يتأثر بالمدة التي يستغرقها الخيط في تنفيذ دالته.
    ///
    ///
    /// ```no_test
    ///     wwwww|sssssssssss|wwwww|sssssssssss|...
    /// ```
    ///
    ///
    /// ‎‫تتعارض هذه الدالة مع دالة `exclusive`، ويكون الإعتبار لآخر دالة منفذة إذا اجتمعتا
    /// ‎‫على وصف واحد.
    pub fn exclusive(&mut self) -> &mut MoidSpec {
        self.inc = false;
        self
    }
    /// ‎‫دالة إنشاء الخيط. `work` هي دالة التكرار.
    pub fn repeat<F>(&self, work: F) -> Result<MoidHandle, String>
    where F: FnMut(), 
          F: Send + 'static {
        
        self.repeat_with_post_cancel(work, || ())
    }
    /// ‎‫دالة إنشاء الخيط. `work` هي دالة التكرار، و`cancled` هي دالة ينفذها الخيط
    /// ‎‫بعد توقفه نتيجة إلغائه.
    pub fn repeat_with_post_cancel<F, G>(&self, work: F, canceled: G) -> Result<MoidHandle, String>
    where F: FnMut(),
          G: FnOnce(),  
          F: Send + 'static,
          G: Send + 'static {
          
        
        use time;
        let now = time::now().to_timespec().sec;
        
        let from = self.from.map(|from| match from {
            Time::Duration(d) => {
                now+d
            },
            Time::Stamp(t) => t
        });
        
        let to = self.to.map(|to| match to {
            Time::Duration(d) => {
                from.unwrap_or(now)+d
            },
            Time::Stamp(t) => t
        });
        rpt(from, to, self.count, self.sleep, self.inc, work, canceled)
    }
}





fn rpt<F, C>(from: Option<i64>, to: Option<i64>, count: Option<usize>, sleep: f64, inc: bool, work: F, cleanup: C) -> Result<MoidHandle, String> 
where F: FnMut(), 
      F: Send + 'static,
      C: FnOnce(), 
      C: Send + 'static  {
    
    use time;
    
    //:‎ ‫أظول فترة أنتظار محدودة بسعة مدخلات park_timeout_ms والذي
    //:‎ ‫يستقبل مدة الإنتظار بالملي-ثانية ونوعه u32؛ أي أطول مدة ممكنة 
    //:‎ ‫هي شهرين. MAX_TIME نصف هذه المدة.
    const MAX_TIME: f64 = 2592000.0;
    
    //:‎ ‫ ‎القناة المستخدمة للإرسال إشارة الإنهاء لخيط تفيذ الحمولة
    let (tx, rx) = mpsc::channel::<()>();
    
    let sleep_time = sleep;
    let mut work = work;
    
    //:‎ ‫حساب الأزمنة نسبةً لـpercise_time_s بدلا من الـepoch
    let percise_now = time::precise_time_s();
    let epoch_now = time::now().to_timespec().sec;
    let percise_start = from.map(|from| percise_now + (from - epoch_now) as f64); //:‎ ‫حين يبدأ التنفيذ
    let percise_stop = to.map(|to| percise_now + (to - epoch_now) as f64); //:‎ ‫حين ينتهي التنفيذ
    
    //:‎ ‫تأكد من عدم خروج عن أقصى مدة إنتظار
    macro_rules! check {($v:expr,$s:expr)=>({
        if  $v >= MAX_TIME {
            return Err(format!(concat!("Maximum wait duration is one month ",
                                "({:.02} seconds), {} seconds ",
                                "where specified for `{}`."), MAX_TIME, 
                                $v, $s));
        }
    })}
    if let Some(percise_start) = percise_start {
        check!(percise_start-percise_now, "from");
    }
    check!(sleep_time, "sleep");
    

    
    let join_handle = thread::spawn(move || {
        let mut current_count = 0;
        loop {
            if let Some(percise_start) = percise_start {
                loop { //:‎ ‫ننتظر دخول وقت التكرار
                    if let Ok(_) = rx.try_recv() {cleanup();return;} //:‎ ‫إشارة الخروج
                    
                    //:‎ ‫فرق المدة (قيمة سالبة تفيد نفاذ المدة)
                    let delta = percise_start-time::precise_time_s();
                    
                    if delta <= 0.0 {
                        break;
                    }
                    else {
                        thread::park_timeout_ms((delta*1000.0) as u32);
                    }
                }
            }
            loop { //:‎ الحلقة الرئيسية
                let mut start_time;
                
                if inc {    //:‎ ‫إذا كانت بداية فترة الإنتظار مع تنفيذ الحمولة
                    start_time = time::precise_time_s();
                    work();
                }
                else {      //:‎ ‫إذا كانت بداية فترة الإنتظار بعد تنفيذ الحمولة
                    work();                    
                    start_time = time::precise_time_s();
                }
                
                //:‎ ‫حلقة المهلة التي تفصل بين التكرار (sleep)
                loop {
                    if let Ok(_) = rx.try_recv() {cleanup();return;} //:‎ ‫إشارة الخروج
                    
                    //:‎ ‫فرق المدة (قيمة سالبة تفيد نفاذ المدة)
                    let delta = (start_time+sleep_time)-time::precise_time_s();
                    if delta <= 0.0 {
                        break;
                    }
                    
                    if let Some(percise_stop) = percise_stop {
                        if time::precise_time_s() >= percise_stop {
                            return;
                        }
                    }
                    
                    thread::park_timeout_ms((delta*1000.0) as u32);
                }
                
                if let Some(count) = count {
                    current_count += 1;
                    if current_count >= count {return;}
                }
                
                
            }
        }
    });
    Ok(MoidHandle {
        join_handle: join_handle,
        signal_tx: tx
    })
}

/// ‎‫كرر الدالة `work` في خيط منفصل كل `sleep` ثانية.
pub fn repeat<F>(sleep: f64, work: F) -> Result<MoidHandle, String> 
where F: FnMut(), 
      F: Send + 'static {
    MoidSpec::new().sleep(sleep).repeat(work)
}

/// ‎‫كرر الدالة `work` ‏`count` مرات على الأكثر في خيط منفصل كل `sleep` ثانية.
pub fn nrepeat<F>(count: usize, sleep: f64, work: F) -> Result<MoidHandle, String> 
where F: FnMut(), 
      F: Send + 'static {
    MoidSpec::new().count(count).sleep(sleep).repeat(work)
}
/// ‎‫نفذ الدالة `work` في خيط منفصل بعد `delay` ثانية.
pub fn delay<F>(delay: i64, work: F) -> Result<MoidHandle, String> 
where F: FnMut(), 
      F: Send + 'static {
    MoidSpec::new().count(1).delay(delay).repeat(work)
}
/// ‎‫كرر تنفيذ الدالة `work` في خيط منفصل.
pub fn spin<F>(work: F) -> Result<MoidHandle, String> 
where F: FnMut(), 
      F: Send + 'static {
    MoidSpec::new().repeat(work)
}

