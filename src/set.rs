/*
    Moid: Cancelable, Detached Loops and Scheduling
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use core::MoidHandle;

/// ‎‫ربط وتحكم جماعي للخيوط.
/// 
/// ‎‫اسقاط هذه المجموعة يتسبب في استيقاف جميع الخيوط المناطة إليها.
pub struct MoidSet {
    set: Vec<MoidHandle>
}

impl MoidSet {
    /// ‎‫تنشئ مجموعة جديدة
    pub fn new() -> MoidSet {
        MoidSet {
            set: vec![]
        }
    }
    /// ‎‫تربط خيط بالمجموعة
    pub fn attach(&mut self, handle: MoidHandle) {
        self.set.push(handle);
    }
    /// ‎‫يستوقف ويزيل جميع خيوط المجموعة
    pub fn cancel(&mut self) {
        use std::mem;
        let set = mem::replace(&mut self.set, vec![]);
        for handle in set.into_iter() {
            handle.cancel();
        }
    }
    /// ‎‫تصد المنفذ إلى أن تنتهي جميع خيوط المجموعة من أعمالها.
    pub fn join(&mut self) {
        use std::mem;
        let set = mem::replace(&mut self.set, vec![]);
        for handle in set.into_iter() {
            handle.join();
        }
    }
}

impl Drop for MoidSet {
    fn drop(&mut self) {
        self.cancel();
    }
}

