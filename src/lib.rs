/*
    Moid: Cancelable, Detached Loops and Scheduling
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or 
    modify it under the terms of the GNU Lesser General Public 
    License as published by the Free Software Foundation, either 
    version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! ‏<div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//! جدولة الزمنية لتنفيذ الدوال المتكرر في خيوط مستقلة قابلة للإيقاف
//!
//! ‏</div><div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//! 
//! ‏نماذج لاستخدام المكتبة: 
//! 
//! ‏</div>
//! 
//! ```
//! use moid;
//!
//! let handle = moid::repeat(0.25, || println!("أُنَفَذُ كُلُ رُبْع ثَانيِة")).unwrap();
//! //‎ أوامر أخرى ...
//! handle.cancel();
//!
//! ```

//! ‏<div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//!
//! ‏في المثال السابق ينفذ البرنامج دالة كل ربع ثانية في خيط مستقل، ويستمر
//! الخيط في تنفيذ الدالة إلى أن ينتهي البرنامج الرئيسي أو يستوقف بتنفيذ
//! دالة cancel على قبضة الخيط. فيما يلي مثال لتنفيذ دالة ٦ مرات بعد حلول
//! ثانيتين على إنشاء الخيط. سيحاول الخيط تنفيذ الدالة في ظرف ١٥٠ ملي ثانية 
//! ويستغرق ما تبقى من زمن في إنتظار الظرف التالي. تنفيذ join على قبضة
//! الخيط توقف الخيط المنفذ إلى أن ينتهي التكرار (كما هو الحال مع JoinHandle).
//! 
//! ‏</div>
//!
//! ```
//! use moid;
//!
//! let mut counter = 0;
//! let handle = moid::MoidSpec::new()
//!     .sleep(0.15)
//!     .delay(2)
//!     .inclusive()
//!     .count(6)
//!     .repeat(move || {
//!         println!("{}", counter);
//!         counter += 1;
//!     })
//!     .unwrap();
//! //‎ أوامر أخرى ...
//! handle.join();
//!
//! ```
//! ‏<div lang="ar" dir="rtl" style="direction:rtl;text-align:right">
//!
//! أوجه القصور: لا يمكن استيقاف خيط التكرار أثناء تنفيذ دالته؛ وذلك بسبب عدم 
//! وجود طريقة لإجبار الخيوط على التوقف في std::thread. قصور آخر  هو 
//! عدم قدرة المكتبة على جدولة مدة الإنتظار لأكثر من شهر .
//! 
//! ‏</div>
//! 
//! # Copyright #
//! 
//! Copyright © 2015  Josh Morin <JoshMorin@gmx.com>
//! 
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Lesser General Public License as published by
//! the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//! 
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Lesser General Public License for more details.
//! 
//! You should have received a copy of the GNU General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.



extern crate time;

mod core;
mod set;

pub use core::*;
pub use set::*;


#[test]
fn it_works() {
    use std::thread;
    
    {
        let mut x = MoidSet::new();
        let mut count = 1;
        x.attach(repeat(0.1, move || println!("count = {}", {count+=1; count})).unwrap());
        x.attach(delay(1, || println!("should show")).unwrap());
        x.attach(delay(2, || panic!("should NOT show")).unwrap());
        thread::sleep_ms(1100);
    }
    
    thread::sleep_ms(200);
       
}
