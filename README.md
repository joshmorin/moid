Moid: Cancelable, Detached Loops and Scheduling
===============================================

‫تعني مكتبة **موعد** بجدولة خيوط مستقلة لتنفيذ الدوال المتكرر القابلة للإيقاف: 
‫المكتبة تمكنك من توقيف الخيوط وتأجيل بدايتها وتحديد نهايتها إما بوقت أو بحد لعدد 
‫التكرار.


# مثال #

    let handle = moid::repeat(0.25, || println!("أُنَفَذُ كُلُ رُبْع ثَانيِة")).unwrap();
    //‎ أوامر أخرى ...
    handle.cancel();



# تصريف #
‫إضف المكتبة لمشروع بإضافته كمتطلب في Cargo.toml:

    [dependencies.moid]
    name = "moid"
    git = "https://joshmorin@bitbucket.org/joshmorin/moid.git"


# توثيق #

‫وثائق [الواجهة البرمجية](https://joshmorin.github.io/documentation/moid). 

‫أنصحك باستخرج التوثيق من المكتبة مباشرة حتى تضمن تطابق إصدار التوثيق مع 
‫المكتبة المستخدمة.


# أعطاب #
‫ارسل التقارير [هنا](https://bitbucket.org/joshmorin/moid/issues/new). 

‫التسجيل غير لازم لمقدمي التقارير. لا مانع من مراسلتي مباشرة على العنوان 
‫JoshMorin@gmx.com (ابدأ العوان بكلمة "عطب").

# ساهم #
ارسل تقريرا توضح فيه الجزئية المراد المساهمة فيه قبل أن تشرع فيه. لا استطيع أن 
أضمن قبول المساهمات ما لم أتحقق من موافقتها لاتجاه التطويري للمكتبة.

# Copyright #
Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
